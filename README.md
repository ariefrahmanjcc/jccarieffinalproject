# PasarKoin

## Deskripsi

Aplikasi ini adalah aplikasi untuk menampilkan parameter aset kripto dan berita terkait aset kripto

## Sumber Data:
- API : api.coincap.io/v2
- API: newsapi.org/v2

###Link Mockup Figma
https://www.figma.com/file/Jbm1ZYUH5nVW32yXFLEJJ6/PasarKoin?node-id=0%3A1

## Kegunaan Aplikasi
- Untuk mengetahui harga aset kripto beserta detailnya, seperti market cap, volume 24%, dll.
- Menampilkan berita-berita seputar aset kripto

## Teknologi yang Digunakan

- Redux state management untuk login dan logout
- Asyncstorage: Untuk menyimpan data JWT agar user otomatis login saat membuka aplikasi
- Expo web browser untuk membuka link web.
- Axios untuk mengambil data (fetching) dari API
- React native svg dan chart kit, untuk plotting chart harga aset kripto setahun kebelakang
- React navigator untuk navigasi halaman
- Ionicon untuk mengambil icon.

## Hasil Aplikasi

- Link Video Demo YouTube/IG Reels
https://youtu.be/7v00nBNbZcg
https://www.instagram.com/reel/CUSddlJJ4Ag/

- Link File APK
https://drive.google.com/file/d/1hqI8fXEwRf8ViW9AziTLhw31ilzxyZ6f/view?usp=sharing
