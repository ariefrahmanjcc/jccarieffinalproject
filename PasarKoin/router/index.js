import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import SignIn from "../screens/SignIn";
import SignUp from "../screens/SignUp";

const Stack = createStackNavigator();

export default function Router() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerTitleAlign: "center",
        headerStyle: {
          backgroundColor: "#00B207",
        },
        headerTintColor: "#FFD700",
        headerTitleStyle: {
          fontWeight: "bold",
        },
      }}
    >
      <Stack.Screen name="MASUK" component={SignIn} />
      <Stack.Screen name="DAFTAR" component={SignUp} />
    </Stack.Navigator>
  );
}
