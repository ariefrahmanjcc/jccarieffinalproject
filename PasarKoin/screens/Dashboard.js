import React, { useState, useEffect } from "react";
import { StyleSheet, Text, View, FlatList, Button } from "react-native";
import axios from "axios";
import { TouchableOpacity } from "react-native-gesture-handler";

export default function Dashboard({ navigation }) {
  const [items, setItems] = useState([]);
  useEffect(() => {
    axios.get("https://api.coincap.io/v2/assets?limit=10").then((res) => {
      const data1 = res.data.data;
      setItems(data1);
    });
  }, []);

  return (
    <View style={styles.container}>
      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-between",
          padding: 10,
        }}
      >
        <Text style={styles.full}>Hello Im Yoon Ah</Text>
        <Button title="refresh" onPress={() => {}} />
      </View>

      <View style={styles.tableHeader}>
        <View style={styles.rowHeader}>
          <View style={styles.columnHeader}>
            <Text style={styles.textHeader}>Koin</Text>
          </View>
          <View style={styles.columnHeader}>
            <Text style={styles.textHeader}>Harga Terakhir</Text>
            <Text style={styles.textHeader}>USD</Text>
          </View>
          <View style={styles.columnHeader}>
            <Text style={styles.textHeader}>Perubahan</Text>
            <Text style={styles.textHeader}>24 Jam</Text>
          </View>
        </View>
      </View>

      <FlatList
        style={styles.container}
        data={items}
        keyExtractor={(item) => item.symbol}
        renderItem={({ item }) => {
          return (
            <TouchableOpacity
              activeOpacity={0.7}
              onPress={() =>
                navigation.navigate("DETAILS", {
                  id: item.id,
                  name: item.name,
                })
              }
            >
              <View style={styles.tableContent}>
                <View style={styles.rowContent}>
                  <View style={styles.columnContent}>
                    <Text style={styles.textContent1}>{item.symbol}</Text>
                    <Text style={styles.textContent2}>{item.name}</Text>
                  </View>
                  <View style={styles.columnContent}>
                    <Text style={styles.textContent3}>
                      {item.priceUsd.slice(0, 7)}
                    </Text>
                  </View>
                  <View style={styles.columnContent}>
                    <Text style={styles.textContent4}>
                      {item.changePercent24Hr.slice(0, 5)}
                    </Text>
                  </View>
                </View>
              </View>
            </TouchableOpacity>
          );
        }}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    //justifyContent: 'center'
  },
  full: {
    fontSize: 20,
    fontWeight: "bold",
  },
  tableHeader: {
    backgroundColor: "#FFD700",
    justifyContent: "center",
    borderRadius: 10,
    paddingHorizontal: 5,
    margin: 5,
  },
  rowHeader: {
    flexDirection: "row",
    paddingVertical: 5,
    justifyContent: "space-between",
    alignItems: "center",
  },
  columnHeader: {
    //flex:1,
    flexDirection: "column",
    paddingVertical: 5,
    alignItems: "flex-start",
  },
  textHeader: {
    color: "#00B207",
    fontSize: 15,
  },
  tableContent: {
    backgroundColor: "white",
    borderRadius: 10,
    paddingHorizontal: 5,
    margin: 5,
  },
  rowContent: {
    flexDirection: "row",
    paddingVertical: 5,
    justifyContent: "space-between",
    alignItems: "center",
  },
  columnContent: {
    flex: 1,
    flexDirection: "column",
    paddingVertical: 5,
    alignItems: "flex-start",
    //backgroundColor: 'yellow'
  },
  columnContentCenter: {
    flexDirection: "column",
    paddingVertical: 5,
    alignItems: "flex-end",
  },
  textContent1: {
    fontSize: 20,
    color: "black",
    fontWeight: "bold",
  },
  textContent2: {
    color: "grey",
  },
  textContent3: {
    color: "black",
    fontSize: 20,
    alignSelf: "center",
  },
  textContent4: {
    color: "black",
    fontSize: 20,
    fontWeight: "bold",
    alignSelf: "flex-end",
    //backgroundColor: { }
  },
});
