import axios from "axios";
import React, { useState, useEffect } from "react";
import { StyleSheet, Text, View, FlatList, Image, Linking } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import * as WebBrowser from "expo-web-browser";

export default function news() {
  const [items, setItems] = useState([]);

  useEffect(() => {
    axios
      .get(
        "https://newsapi.org/v2/everything?q=kripto&language=id&apiKey=aa60a2271c954db5bd9bef1f058b721f"
      )

      .then((res) => {
        const data1 = res.data.articles;
        console.log("res = ", data1);
        setItems(data1);
      });
  }, []);

  return (
    <View style={styles.container}>
      <View>
        <FlatList
          data={items}
          keyExtractor={(item) => item.title}
          renderItem={({ item }) => {
            return (
              <TouchableOpacity
                activeOpacity={0.7}
                onPress={() => WebBrowser.openBrowserAsync(`${item.url}`)}
              >
                <View style={styles.mainBox}>
                  <Image
                    style={styles.imageThumb}
                    source={{ uri: `${item.urlToImage}` }}
                  />

                  <View style={styles.descBox}>
                    <Text style={{ fontWeight: "bold" }}>{item.title}</Text>
                    <Text>{item.description}</Text>
                  </View>
                </View>
              </TouchableOpacity>
            );
          }}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  mainBox: {
    flexDirection: "row",
    padding: 5,
    borderWidth: 1,
    borderColor: "#00B207",
  },
  imageThumb: {
    width: 100,
    height: 100,
    borderRadius: 10,
  },
  descBox: {
    flex: 1,
    flexDirection: "column",
    paddingLeft: 10,
  },
});
