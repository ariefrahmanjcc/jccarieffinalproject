import React from "react";
import { StyleSheet, Text, View, Image, TouchableOpacity } from "react-native";
import * as WebBrowser from "expo-web-browser";

export default function AboutMe() {
  return (
    <View style={styles.container}>
      <View style={styles.logoapp}>
        <Image source={require("../assets/logoApp.png")} />
        <Text style={{ color: "grey" }}>V 1.0</Text>
      </View>
      <Text style={styles.textSubHeader}>Developer</Text>
      <View style={styles.box}>
        <View style={styles.column1}>
          <Image source={require("../assets/pp.png")} />
          <View style={styles.column2}>
            <Text style={styles.name}>Arief Rahman</Text>
            <Text style={styles.occupation}>React Native Developer</Text>
          </View>
        </View>
      </View>

      <Text style={styles.textSubHeader}>Contact Me</Text>
      <TouchableOpacity
        style={styles.box}
        activeOpacity={0.7}
        onPress={() => {}}
      >
        <View style={styles.column1}>
          <Image source={require("../assets/email.ico.png")} />
          <View style={styles.column2}>
            <Text>Kirim Email</Text>
          </View>
        </View>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.box}
        activeOpacity={0.7}
        onPress={() => WebBrowser.openBrowserAsync(`mailto: arieef.rahman.6791@gmail.com`)}
      >
        <View style={styles.column1}>
          <Image source={require("../assets/gitlab.ico.png")} />
          <View style={styles.column2}>
            <Text>Portofolio</Text>
          </View>
        </View>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.box}
        activeOpacity={0.7}
        onPress={() => WebBrowser.openBrowserAsync(`https://instagram.com/arief.rahman.6791`)}
      >
        <View style={styles.column1}>
          <Image source={require("../assets/insta.ico.png")} />
          <View style={styles.column2}>
            <Text>Follow Instagram</Text>
          </View>
        </View>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.box}
        activeOpacity={0.7}
        onPress={() => WebBrowser.openBrowserAsync(`https://twitter.com/onestophallyu/`)}
      >
        <View style={styles.column1}>
          <Image source={require("../assets/twit.ico.png")} />
          <View style={styles.column2}>
            <Text>Follow Twitter</Text>
          </View>
        </View>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  logoapp: {
    alignItems: "center",
    marginVertical: 30,
  },
  textSubHeader: {
    fontSize: 30,
    fontWeight: "bold",
    alignSelf: "center",
  },
  box: {
    flexDirection: "column",
    backgroundColor: "#FFFAE0",
    justifyContent: "center",
    borderWidth: 1,
    borderColor: "#00b207",
    borderRadius: 10,
    paddingHorizontal: 5,
    margin: 5,
  },
  column1: {
    flexDirection: "row",
    paddingVertical: 5,
  },
  column2: {
    flexDirection: "column",
    paddingLeft: 5,
  },
  name: {
    fontSize: 30,
  },
  occupation: {
    fontSize: 20,
    fontStyle: "italic",
  },
});
