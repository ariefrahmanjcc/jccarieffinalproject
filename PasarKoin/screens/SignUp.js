import React, { useState } from "react";
import { useContext } from "react";
import {
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from "react-native";
import logo from "../assets/logo.png";
import { AuthContext } from "../context/AuthContext";

export default function SignUp({ navigation }) {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const { signUp } = useContext(AuthContext);

  return (
    <View style={styles.container}>
      <Image source={logo} style={styles.logo} />
      <View style={styles.form}>
        <TextInput
          value={email}
          style={styles.input}
          placeholder="Masukkan Email..."
          onChangeText={(email) => {
            setEmail(email);
          }}
        />
        <TextInput
          value={password}
          style={styles.input}
          placeholder="Masukkan Password..."
          onChangeText={(password) => {
            setPassword(password);
          }}
        />
      </View>
      <View>
        <TouchableOpacity
          style={styles.button1}
          activeOpacity={0.7}
          onPress={() => signUp(email, password)}
        >
          <Text style={styles.buttonText1}>DAFTAR</Text>
        </TouchableOpacity>
        <Text style={{ alignSelf: "center" }}> sudah punya akun? </Text>
        <TouchableOpacity
          style={styles.button2}
          activeOpacity={0.7}
          onPress={() =>
            navigation.reset({
              index: 0,
              routes: [{ name: "MASUK" }],
            })
          }
        >
          <Text style={styles.buttonText2}>MASUK</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    justifyContent: "space-between",
  },
  logo: {
    top: 10,
    alignSelf: "center",
  },
  form: {},
  input: {
    backgroundColor: "#FFFAE0",
    borderRadius: 10,
    margin: 5,
    padding: 10,
  },
  button1: {
    padding: 10,
    margin: 5,
    borderRadius: 10,
    backgroundColor: "#00B207",
  },
  buttonText1: {
    color: "#FFD700",
    fontWeight: "bold",
    alignSelf: "center",
  },
  button2: {
    padding: 9,
    margin: 5,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "#00B207",
    backgroundColor: "#FFFAE0",
  },
  buttonText2: {
    color: "#00B207",
    fontWeight: "bold",
    alignSelf: "center",
  },
});
