import axios from "axios";
import React, { useState, useEffect } from "react";
import { StyleSheet, Text, View } from "react-native";
import { LineChart } from "react-native-chart-kit";
import { Dimensions } from "react-native";

export default function CoinDetails({ route }) {
  const [items, setItems] = useState([]);
  const [items2, setItems2] = useState([]);
  const { id } = route.params;

  useEffect(() => {
    axios.get(`https://api.coincap.io/v2/assets/${id}`).then((res) => {
      const data1 = res.data.data;
      console.log(data1);
      setItems(data1);
      axios
        .get(`https://api.coincap.io/v2/assets/${id}/history?interval=d1`)
        .then((res2) => {
          const data2 = res2.data.data;
          console.log(data2);
          setItems2(data2);
        });
    });
  }, []);

  return (
    <View style={styles.container}>
      <View style={styles.priceVolume}>
        <View style={styles.pvContent}>
          <Text style={styles.textPrice}>{items.priceUsd?.slice(0, 7)}</Text>
          <Text style={styles.textOther}>
            {items.changePercent24Hr?.slice(0, 5)}
          </Text>
        </View>
        <View style={styles.pvContent}>
          <Text style={styles.textOther}>VOL 24 jam (USD)</Text>
          <Text style={styles.textOther}>
            {items.volumeUsd24Hr?.slice(0, 10)}
          </Text>
        </View>
      </View>
      <View>
        <Text style={{ fontSize: 20, fontWeight: "bold", alignSelf: "center" }}>
          Daily Chart
        </Text>
        <LineChart
          data={{
            labels: [],
            datasets: [
              {
                data: items2.map((item) => item.priceUsd),
              },
            ],
          }}
          width={Dimensions.get("window").width} // from react-native
          height={220}
          withDots={false}
          withShadow={false}
          withInnerLines={false}
          yAxisLabel="$"
          //yAxisSuffix="k"
          yAxisInterval={1} // optional, defaults to 1
          chartConfig={{
            strokeWidth: 2,
            backgroundGradientFrom: "white",
            backgroundGradientTo: "white",
            decimalPlaces: 2, // optional, defaults to 2dp
            color: (opacity = 1) => `rgba(0, 178, 7, ${opacity})`,
            labelColor: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
            style: {
              borderRadius: 10,
            },
          }}
          bezier
          style={{
            marginVertical: 5,
            borderRadius: 10,
          }}
        />
      </View>
      <Text style={{ fontSize: 20, fontWeight: "bold", alignSelf: "center" }}>
        Info
      </Text>
      <View style={styles.priceVolume}>
        <View style={styles.pvContent}>
          <Text style={{ color: "grey" }}>suplai :</Text>
          <Text style={{ color: "grey" }}>suplai maksimum :</Text>
          <Text style={{ color: "grey" }}>Kapitalisasi pasar:</Text>
        </View>
        <View style={styles.pvContent}>
          <Text>{items.supply?.slice(0, 12)}</Text>
          <Text>{items.maxSupply?.slice(0, 12)}</Text>
          <Text>{items.marketCapUsd?.slice(0, 12)}</Text>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //alignItems: 'center'
  },
  priceVolume: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    padding: 10,
  },
  pvContent: {
    alignItems: "flex-start",
  },
  textOther: {
    fontSize: 18,
    fontWeight: "bold",
  },
  textPrice: {
    fontSize: 30,
    fontWeight: "bold",
  },
});
